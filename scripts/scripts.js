class SnowCatch {

	constructor() {
		this.snowFlake = document.getElementById('Capa_1'); // https://learn.javascript.ru/searching-elements-dom#document-getelementbyid-ili-prosto-id
		this.startButton = document.getElementById('startButton');
		this.gameCanvas = document.getElementById('gameCanvas');
		this.amountText = document.getElementById('amountText');
		this.timeText = document.getElementById('timeText');
		this.gameTime = 30;
		this.init();
		this.SIZEX = 270;
		this.SIZEY = 270;
	}

	init() {
		this.attachEvents();
	}

	attachEvents() {
		this.startButton.addEventListener('click', () => { // https://learn.javascript.ru/introduction-browser-events#addeventlistener
			this.start();
		});
	}

	setSnowRandomPosition(e) {
		let x = Math.round(Math.random() * this.SIZEX);
		let y = Math.round(Math.random() * this.SIZEY);
		if (e && e.x && e.y) {
			while (x>=e.x && x<=(e.x+20)) {
				x = Math.round(Math.random() * this.SIZEX);
			}
			while (y>=e.y && y<=(e.y+20)) {
				y = Math.round(Math.random() * this.SIZEY);
			}
		}
		this.snowFlake.style.top = y; // https://learn.javascript.ru/styles-and-classes#element-style
		this.snowFlake.style.left = x;
	}

	start() {
		this.startGameTimer();
		let amount = 0;
		this.snowFlake.style.display = 'block';
		this.gameCanvas.style.opacity = '1';

		this.setSnowRandomPosition();

		this.snowFlake.addEventListener('click', () => {
			this.setSnowRandomPosition(e);
			amount++;
			this.amountText.style.visibility = 'visible';
			this.amountText.textContent = `You caught - ${amount}`; // https://learn.javascript.ru/basic-dom-node-properties#textcontent-prosto-tekst
		});

		this.snowFlake.addEventListener('mouseover', (e) => {
			clearTimeout(this.snowInterval);
			this.snowInterval = setTimeout(() => {
				this.setSnowRandomPosition(e);
			}, 1)
		});
	}

	startGameTimer() {
		this.timeText.style.visibility = 'visible';
		this.timeText.textContent = `You have ${this.gameTime}`;
		let startTime = new Date().getTime();

		this.gameInterval = setInterval(() => {
			let currentTime = new Date().getTime();
			let difference = Math.round((currentTime - startTime) / 1000);

			let leftTime = this.gameTime - difference;

			if (this.gameTime > difference) {
				this.timeText.textContent = `You have ${leftTime}`;
			} else {
				this.stop();
			}
		}, 1000);
	}

	stop() {
		this.timeText.textContent = 'Time left';
		this.snowFlake.style.display = 'none';
		this.gameCanvas.style.opacity = '.75';
		clearInterval(this.gameInterval);
	}
}

new SnowCatch();
